# Bropackreload
### Story and Motivation
Tristan world builds here!@!

### Ways to get the mod files
Some of the configured ways to grab the latest files from the master branch. These will be the latest updates in most cases. Click the download link for your specific uses.

#### Gets Forge + Mods in server configuation
https://gitlab.com/mortalterminal/bropackreload/-/jobs/artifacts/v0.2.7-rc2/download?job=deploy-server

#### Gets just the Mods Folder
https://gitlab.com/mortalterminal/bropackreload/-/jobs/artifacts/v0.2.7-rc2/download?job=deploy-mods

#### Gets the Forge Client + Mods 
https://gitlab.com/mortalterminal/bropackreload/-/jobs/artifacts/v0.2.7-rc2/download?job=deploy-client

#### Gets the modlist
https://gitlab.com/mortalterminal/bropackreload/-/jobs/artifacts/v0.2.7-rc2/raw/Forge/modlist?job=deploy-client

## Mod List with Forge Pages: 
#### Industrial side  

Buildcraft: https://mod-buildcraft.com/pages/download.html  
IC2: https://www.curseforge.com/minecraft/mc-mods/industrial-craft/files/2746892  
AE2Stuff: https://www.curseforge.com/minecraft/mc-mods/ae2-stuff/files/2491032 (Beta)  
AE2: https://www.curseforge.com/minecraft/mc-mods/applied-energistics-2/files/2747063  
AE2Wireless: https://www.curseforge.com/minecraft/mc-modswireless-crafting-terminal/files    
Immersive-engineering:https://www.curseforge.com/minecraft/mc-mods/immersive-engineering/files/2676501  
Railcraft: https://www.curseforge.com/minecraft/mc-mods/railcraft/files  
EnderIO: https://www.curseforge.com/minecraft/mc-mods/ender-io/files  
Thermal F: https://www.curseforge.com/minecraft/mc-mods/thermal-foundation/files  
Thermal E: https://www.curseforge.com/minecraft/mc-mods/thermal-expansion/files  
Thermal D: https://www.curseforge.com/minecraft/mc-mods/thermal-dynamics/files  
Tinkers: https://www.curseforge.com/minecraft/mc-mods/tinkers-construct/files  
Tinkers T.L: https://www.curseforge.com/minecraft/mc-mods/tinkers-tool-leveling/files  
ProjectRed: https://www.curseforge.com/minecraft/mc-mods/project-red-base/files  
ProjectRed I: https://www.curseforge.com/minecraft/mc-mods/project-red-integration/files  
ProjectRed F: https://www.curseforge.com/minecraft/mc-mods/project-red-fabrication/files  
ProjectRed M: https://www.curseforge.com/minecraft/mc-mods/project-red-mechanical/files  
ProjectRed L: https://www.curseforge.com/minecraft/mc-mods/project-red-lighting/files  
ProjectRed W: https://www.curseforge.com/minecraft/mc-mods/project-red-world/files  
ProjectRed C: https://www.curseforge.com/minecraft/mc-mods/project-red-compat/files  
Galacticraft: https://micdoodle8.com/mods/galacticraft/downloads  
Galacticraft P: https://micdoodle8.com/mods/galacticraft/downloads  
Adv Generators: https://www.curseforge.com/minecraft/mc-mods/advanced-generators/files (Beta)  
Comp. Solar: https://www.curseforge.com/minecraft/mc-mods/compact-solars/files (Beta)  
Compactor: https://www.curseforge.com/minecraft/mc-mods/compacter/files (Beta)  
Adv Mach: https://www.curseforge.com/minecraft/mc-mods/advanced-machines/files   
Energy Converter: https://www.curseforge.com/minecraft/mc-mods/energy-converters/files  
Stargate: https://www.curseforge.com/minecraft/mc-mods/stargate-network/files  
OpenComputers: https://www.curseforge.com/minecraft/mc-mods/opencomputers/files  



#### Magic  
Blood magic: https://www.curseforge.com/minecraft/mc-mods/blood-magic (Beta)  
ProjectE:https://www.curseforge.com/minecraft/mc-mods/projecte/files/2702991  
Astral sorcery:https://www.curseforge.com/minecraft/mc-mods/astral-sorcery  
Thaumcraft: https://www.curseforge.com/minecraft/mc-mods/thaumcraft (Beta)  

#### Quality of life changes  
Ironchest:https://www.curseforge.com/minecraft/mc-mods/iron-chests/files/2747935 (Beta)  
InventoryTweaks:https://www.curseforge.com/minecraft/mc-mods/inventory-tweaks/files/2482481  
Bibliocraft:https://www.curseforge.com/minecraft/mc-mods/bibliocraft/files/2574880  
Ender Storage: https://www.curseforge.com/minecraft/mc-mods/ender-storage-1-8/files  
Quick Leaf Decay: https://www.curseforge.com/minecraft/mc-mods/quick-leaf-decay/files  
Storage Drawers: https://www.curseforge.com/minecraft/mc-mods/storage-drawers/files   
Storage Drawers Extras: https://www.curseforge.com/minecraft/mc-mods/storage-drawers-extras/files    
The Wierding Gaget: https://www.curseforge.com/minecraft/mc-mods/the-weirding-gadget/files  
VM: https://www.curseforge.com/minecraft/mc-mods/veinminer/files  
WAWLA: https://www.curseforge.com/minecraft/mc-mods/wawla-what-are-we-looking-at/files  
WAWLA H: https://www.curseforge.com/minecraft/mc-mods/waila-harvestability/files  
Recipe Conflict: https://www.curseforge.com/minecraft/mc-mods/stimmedcow-nomorerecipeconflict/files  
OpenBlocks: https://www.curseforge.com/minecraft/mc-mods/openblocks/files  
Bonsai Trees: https://www.curseforge.com/minecraft/mc-mods/bonsai-trees/files  
Littletiles: https://www.curseforge.com/minecraft/mc-mods/littletiles  
HWYLA: https://www.curseforge.com/minecraft/mc-mods/hwyla/files?sort=releasetype  
JourneyMap: https://www.curseforge.com/minecraft/mc-mods/journeymap/files  
SecretRooms:https://www.curseforge.com/minecraft/mc-mods/secretroomsmod  
Keyboard Wizard: https://www.curseforge.com/minecraft/mc-mods/keyboard-wizard/files  

#### Exploration     
Airships: https://www.curseforge.com/minecraft/mc-mods/valkyrien-warfare/files  
Inventory pets: https://www.curseforge.com/minecraft/mc-mods/inventory-pets  
Rougelike Dungeons: https://www.curseforge.com/minecraft/mc-mods/roguelike-dungeons/files  
Enderetic expension (end dimension) :https://www.curseforge.com/minecraft/mc-mods/endergetic  

#### Core  
Baubles: https://www.curseforge.com/minecraft/mc-mods/baubles  
EnderCore: https://www.curseforge.com/minecraft/mc-mods/endercore/files  
JEI: https://www.curseforge.com/minecraft/mc-mods/jei/files  
JER: https://www.curseforge.com/minecraft/mc-mods/just-enough-resources-jer/files  
NEI: https://www.curseforge.com/minecraft/mc-mods/not-enough-items-1-8/files  
Unidic: https://www.curseforge.com/minecraft/mc-mods/unidict/files  
COFH core: https://www.curseforge.com/minecraft/mc-mods/cofh-core/files  
COFH world: https://www.curseforge.com/minecraft/mc-mods/cofh-world/files  
Chameleon: https://www.curseforge.com/minecraft/mc-mods/chameleon/files  
Mantle: https://www.curseforge.com/minecraft/mc-mods/mantle/files  
WanionLib: https://www.curseforge.com/minecraft/mc-mods/wanionlib/files  
CodeChickenLib: https://www.curseforge.com/minecraft/mc-mods/codechicken-lib-1-8/files  
BdLib: https://www.curseforge.com/minecraft/mc-mods/bdlib/files (Beta)  
OpenModsLib: https://www.curseforge.com/minecraft/mc-mods/openmodslib/files  
Foamfix:https://www.curseforge.com/minecraft/mc-mods/foamfix-for-minecraft/files/2695388  
Redstone Flux API: https://www.curseforge.com/minecraft/mc-mods/redstone-flux  
Guide API: https://www.curseforge.com/minecraft/mc-mods/guide-api/files  
MrTJPCore: https://www.curseforge.com/minecraft/mc-mods/mrtjpcore/files  
Creative_core: https://www.curseforge.com/minecraft/mc-mods/creativecore/files  
Optifine: https://optifine.net/downloads  
MalisisCore: https://www.curseforge.com/minecraft/mc-mods/malisiscore/files  
ForgeMultiPart: https://www.curseforge.com/minecraft/mc-mods/forge-multipart-cbe/files  
MicdoodleCore: https://micdoodle8.com/mods/galacticraft/downloads  