# Contributing
Contributers may want to have a working knowlage of minecraft mods and modpack 
technequtes. This project also assumes some proficency in gitlab-ci if configuring the CI/CD steps.

Keep in mind that when wanting to deploy a new version of the pack the CI will only
pickup the changes on the last tag. `Master` must be taged with a tag that matches 
the semantic version scheme. Such as
```
v0.0.1
```
or 
```
v1.0.0-rc.1
```

One must also push these tags to this remote repositor in order for the CI to run
successfuly. More info on tagging can be found [here.](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)